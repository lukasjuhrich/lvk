from setuptools import setup, find_packages


NAME = 'tudmath-lvk'
DESCRIPTION = 'A web scraper for the TU Dresden mathematics event catalogue' \
              ' (“Lehrveranstaltungskatalog”)'
URL = 'https://gitlab.com/lukasjuhrich/lvk'
EMAIL = 'lukas.juhrich@agdsn.de'
AUTHOR = 'Lukas Juhrich'
REQUIRES_PYTHON = '>=3.7.0'
VERSION = '0.1'


setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    # long_description="",
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,

    packages=find_packages(exclude=["tests", "*.tests", "*.tests.*", "tests.*"]),
    entry_points="""
        [console_scripts]
        lvkscrape=lvk.cli:cli
    """,

    install_requires=[
        'Click',
        'requests',
        'bs4',
    ],
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy'
    ],
)
