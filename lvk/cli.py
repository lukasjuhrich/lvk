import logging
from typing import Optional

import click
from bs4 import BeautifulSoup
from bs4.element import Tag
from requests import get

from lvk.parsing import LOGGER_NAME, extract_lvs


def get_source(url: str) -> Tag:
    return BeautifulSoup(get(url).content.decode('latin1'), 'html.parser')


@click.command()
@click.option('--debug', '-d', is_flag=True,
              help="Will raise the loglevel to DEBUG")
@click.option('--filter', '-f',
              help="Will grep the module name for that")
@click.option('--limit', '-l', default=10,
              help="Will limit printed results to the given number")
@click.argument('url')
def cli(debug: bool, url: str, filter: Optional[str], limit: int):
    logging.basicConfig()
    if debug:
        logging.getLogger(LOGGER_NAME).setLevel(logging.DEBUG)

    lvs = [lv for lv in extract_lvs(get_source(url))
           if not filter or filter.lower() in lv.titel.lower()]

    if not lvs:
        click.echo("No results.")
        return

    if len(lvs) > limit:
        click.echo(f"Found {len(lvs)} matches, printing the first {limit} only.")

    for lv in lvs[:limit]:
        click.echo(lv.titel)
        click.echo(str(lv.timetable))
        click.echo()
