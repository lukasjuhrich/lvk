import re
from typing import TypeVar, Iterable, Callable, Generator, List

T = TypeVar('T')


def zip_pivot_elements(elements: Iterable[T], qualifier: Callable[[T], bool]) \
        -> Generator[List[T], None, None]:
    current_list: List[T] = []

    for el in elements:
        if qualifier(el) and current_list:
            yield current_list.copy()
            current_list.clear()
        current_list.append(el)


def sanitize_string(s: str) -> str:
    return re.sub(r"\W+", " ", s.strip())