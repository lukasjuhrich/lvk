from __future__ import annotations

import logging
from dataclasses import dataclass
from typing import Iterable, List, NewType, Optional, \
    Tuple, cast

from bs4.element import Tag

from lvk.util import zip_pivot_elements, sanitize_string

LOGGER_NAME = 'lvk_parser'
logger = logging.getLogger(LOGGER_NAME)


def iter_event_fragments(page: Tag) -> Iterable[Event]:
    tables = page.find_all('table', class_='lvspezial')

    def _is_metadata_table(t: Tag) -> bool:
        """Check if the table Tag has a module description (``<strong>``)"""
        return t.strong is not None

    for tables in list(zip_pivot_elements(tables, _is_metadata_table)):
        event_im = parse_pivoted_tables(tables)
        if event_im is None:
            logger.warning("Could not parse tables element")
            continue
        yield event_im


def get_metadata_key(metadata: Tag, key, logger=logger) -> str:
    logger.debug("Getting key %s", key)
    try:
        field = next(el for el in metadata.find_all('td')
                     if key in el.text.lower())
    except StopIteration:
        return ''
    siblings = field.find_next_siblings('td')
    if not siblings:
        return ''
    return siblings[-1].text


PersonInfo = NewType('PersonInfo', str)  # may be replaced by a proper class


def parse_personinfo(td: Tag) -> PersonInfo:
    return cast(PersonInfo, sanitize_string(td.text))


Location = NewType('Location', str)


def parse_location(td: Tag) -> Location:
    return cast(Location, sanitize_string(td.text))


@dataclass
class TimeTableRow:
    person: PersonInfo
    type_: str
    time: Tuple[str, str]  # day, timeslot  # TODO new type
    location: str
    rem_odd_even: str
    rem_format: str
    rem_changes: str

    def __str__(self):
        return " / ".join([self.person, self.type_, str(self.time),
                          self.location, self.rem_odd_even,
                          self.rem_format, self.rem_changes])


def maybe_parse_timetable_row(row: Tag) -> Optional[TimeTableRow]:
    cols: List[Tag] = row.find_all('td')
    if len(cols) != 9:
        return None
    # first col is “Dozent*In/Zeit/Ort” in first row, else empty
    _, person, type_, day, timeslot, location, \
        rem_odd_even, rem_format, rem_changes = cols

    s = sanitize_string
    return TimeTableRow(
        person=parse_personinfo(person),
        type_=s(type_.text),
        time=(s(day.text), s(timeslot.text)),
        location=parse_location(location),
        rem_odd_even=s(rem_odd_even.text),
        rem_format=s(rem_format.text),
        rem_changes=s(rem_changes.text),
    )


class TimeTable(List[TimeTableRow]):
    def __str__(self):
        return "\n".join(str(r) for r in self)


def parse_timetable(timetable: Tag) -> TimeTable:
    rows = TimeTable()
    for row in timetable.find_all('tr'):
        parsed = maybe_parse_timetable_row(row)
        if parsed is None:
            continue
        rows.append(parsed)
    return rows


Event = NewType('Event', Tuple[Tag, Tag])


def parse_pivoted_tables(table_tags: List[Tag], logger=logger) \
        -> Optional[Event]:
    try:
        metadata, timetable, *rest = table_tags
        # unpacking apperently does not work with type hints
        assert isinstance(metadata, Tag)
        assert isinstance(timetable, Tag)
    except ValueError:
        logger.warning("Expected two Tags in constructor, got %d", len(table_tags))
        return None

    if rest:
        logger.warning("Got more than two tables for event")
        logger.debug("Table content: %s", rest)

    return cast(Event, (metadata, timetable))


@dataclass
class Lehrveranstaltung:
    titel: str
    zielgruppe: str
    vorkenntnisse: str
    inhalt: str
    einschreibung: str
    internet: str
    timetable: TimeTable


def parse_lv(metadata: Tag, timetable: Tag) -> Lehrveranstaltung:
    s = sanitize_string
    return Lehrveranstaltung(
        titel=s(metadata.strong.text),
        internet=s(get_metadata_key(metadata, 'internet')),
        einschreibung=s(get_metadata_key(metadata, 'einschreibung')),
        vorkenntnisse=s(get_metadata_key(metadata, 'vorkenntnisse')),
        inhalt=s(get_metadata_key(metadata, 'inhalt')),
        zielgruppe=s(get_metadata_key(metadata, 'zielgruppe')),
        timetable=parse_timetable(timetable)
    )


def extract_lvs(source: Tag) -> Iterable[Lehrveranstaltung]:
    return (parse_lv(meta, timetable)
            for meta, timetable in iter_event_fragments(source))
